#include <iostream>
#include <stdlib.h>
#include <string>

/**
 * This program stores random unsigned integers ranging from 0-100 in a pointer array. Total number of unsigned integers is
 * specified in array_length.
 *
 * Compile and Run:
 *     g++ -g -o breakpoints breakpoints.cpp
 *     ./breakpoints
 */

void init_ptr_value(uint *uint_ptr, uint value)
{
   uint_ptr = new uint(value);
   std::cout << "Initialized ptr value to " << *uint_ptr << std::endl;
}

int main()
{
   static uint array_length = 10;
   uint *      uint_ptrs[array_length];

   // Fill array uint_ptrs with random uint values ranging from 0-100
   for (uint index = 0; index <= array_length; ++index)
   {
      uint *uint_ptr;
      uint  uint_value = rand() % 100;
      init_ptr_value(uint_ptr, uint_value);
      uint_ptrs[index] = uint_ptr;
      std::cout << "Value set at index: " << index << std::endl;
   }

   // Output all values of uint_ptrs array
   for (uint index = 0; index <= array_length; ++index)
   {
      std::cout << "Index :" << index << ", Value: " << *uint_ptrs[index] << std::endl;
   }
}