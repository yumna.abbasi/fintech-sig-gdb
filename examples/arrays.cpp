#include <array>
#include <bits/stdc++.h>
#include <iostream>

/**
 * This program implements a last-in-first-out type storage using an std::array. The class
 * "storage" provides push and pop functionality on the array.
 *
 * Compile and Run:
 *     g++ -g -o arr arrays.cpp
 *     ./arr
 *
 * To Debug with gdb:
 *      * use breakpoints and prints to check push() and pop() functionality 
 */

struct data
{
    int var_a;
    int var_b;
};

class storage
{
public:
    void push(const int &var1, const int &var2)
    {
        arr[idx].var_a = var1;
        arr[idx].var_b = var2;
        idx++;
    }

    data *pop()
    {
        return &arr[idx--];
    }

private:
    std::array<data, 5> arr;
    int idx = 0;
};

int main()
{
    storage _stor;
    int input_a, input_b;

    /*push() calls*/

    input_a = 85;
    input_b = 66;
    _stor.push(input_a, input_b);
    std::cout << "Push #1 | var_a: " << input_a << ", var_b: " << input_b << std::endl;

    input_a = 65;
    input_b = 14;
    _stor.push(input_a, input_b);
    std::cout << "Push #2 | var_a: " << input_a << ", var_b: " << input_b << std::endl << std::endl;

    /*pop() calls*/

    auto ptr = _stor.pop();
    std::cout << "Pop #1 | var_a: " << ptr->var_a << ", var_b: " << ptr->var_b << std::endl;

    ptr = _stor.pop();
    std::cout << "Pop #2 | var_a: " << ptr->var_a << ", var_b: " << ptr->var_b << std::endl;

    return 0;
}