#include <bits/stdc++.h>
#include <iostream>
#include <vector>

/**
 * This program calculates the moving average of N number of integers. The integers are
 * stored in an std::vector and the compute_average() functions takes the vector as input.
 * 
 * To compile and run:
 *      g++ -g -o avg moving_average.cpp
 *      ./avg
 *
 * To Debug using gdb:
 *     * Use conditional breakpoints and prints
 */

void set_size(double* size);

double compute_average(const std::vector<int> &vec) 
{
  double sum = 0;
  double avg = 0;
  double size = 1;

  bool print_list = false;
  bool use_std_sum = false;

  if (!use_std_sum) 
  {
    for (auto &itr : vec)
    {
      if (print_list) 
      {
        std::cout << "" << itr << ",";
      }
      if (itr != 0) 
      {
        sum = sum + itr;
      } else {
        //
      }
      size = vec.size();
      set_size(&size);
      avg = sum / size;
    }    
  }
  else
  {
      avg = (double)std::accumulate(vec.begin(), vec.end(), 0)/ (double)vec.size();
  }
  return avg;
}

void set_size(double* size)
{
  *size = ((int)*size)%6;
}

int main() {

  std::vector<int> input_vec = {0};
  int l_size = 2;
  double sum = 0;
  double mAvg = 0;

  for (int i = 1; i <= 12; i++) {
    input_vec.push_back(i);
    std::cout << "Moving avg of " << i+1
              << " elements : " << compute_average(input_vec) << std::endl;
  }
  return 0;
}