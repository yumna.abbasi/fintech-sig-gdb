#include <cmath>
#include <iostream>

/**
 * This program calculates the factorial of any integer.
 * 
 * To compile and run:
 *      g++ -g -o fact factorial.cpp
 *      ./fact
 * 
 *  To Debug using gdb:
 *     * Use conditional breakpoints and prints
 */

class factorial
{
public:
  int compute_factorial(int num)
  {
    Int prod = 1;
    for (int j = 1; j <= num; j++)
    {
      prod = prod * j;
    }
    fact = prod;

    return fact;
  }

private:

  using Short = uint16_t;
  using Int = uint8_t;
  using Long = uint64_t;

  Short prod = 1;
  Int fact = 1;
};

int main()
{
  factorial fact;
  for (int i = 0; i < 8; i++)
  {
    std::cout << "factorial of " << i << " = " << fact.compute_factorial(i)
              << std::endl;
  }
}