# Fintech SIG - GDB

C++ activity codes to demonstrate GDB usage.
All codes are in `examples` directory

----
### To build in debug mode

``` -bash
g++ -g -o <executable name> <filename.cpp>
```

### To run with gdb
``` -bash
gdb <executable name>
```
